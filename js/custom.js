jQuery(document).ready(function ($) {
  new WOW().init();


  $.each($(".menu-item-has-children"), function (i, d) {
    $(d).append(`
          <button class="dropdown-toggle">
              <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </button>
      `)
  });
  $(".navigation-wrapper").click(function (e) {
    e.preventDefault();

    $(".menu-wrapper").toggleClass("active");
  });
  $("button.close-button").click(function (e) {
    e.preventDefault();

    $(".menu-wrapper").removeClass("active");
  });
  // $(".main-navigation ul li a ").click(function (e) {
  //   e.preventDefault();
  //    $(this).addClass('active');
  //    $(this).first().find('.sub-menu').addClass('active');

  // });
  $(" .dropdown-toggle").click(function (e) {
    e.preventDefault();

  });


  $(".banner-slider").slick({
    autoplay: true,
    dots: false,
    arrows: true,
    infinite: true,
    fade: true,

    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          dots: true,
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,

        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          vertical: false


        }
      },
      {
        breakpoint: 480,
        settings: {
          autoplay: false,
          vertical: false,
          centerMode: true,
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  // $(".team-slider").slick({
  //   autoplay: false,
  //   dots: true,
  //   arrows: false,
  //   infinite: true,
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   responsive: [
  //     {
  //       breakpoint: 1024,
  //       settings: {
  //         dots: true,
  //         slidesToShow: 3,
  //         slidesToScroll: 1,
  //         infinite: true,

  //       }
  //     },
  //     {
  //       breakpoint: 991,
  //       settings: {
  //         slidesToShow: 2,
  //         slidesToScroll: 1,
  //         vertical: false


  //       }
  //     },
  //     {
  //       breakpoint: 480,
  //       settings: {
  //         autoplay: false,
  //         vertical: false,
  //         centerMode: true,
  //         arrows: false,
  //         slidesToShow: 1,
  //         slidesToScroll: 1
  //       }
  //     }
  //   ]
  // });
});